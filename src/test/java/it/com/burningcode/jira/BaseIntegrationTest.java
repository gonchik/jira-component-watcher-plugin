package it.com.burningcode.jira;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.Form;
import com.meterware.httpunit.WebForm;

import javax.inject.Inject;

import static it.com.burningcode.jira.IntegrationTestHelper.*;

/**
 * Created by rbarham on 7/10/17.
 */
public class BaseIntegrationTest extends BaseJiraFuncTest {
    @Inject
    protected Form form;

    public void gotoEditUserWatchersScreen(final long componentId) {
        tester.clickLink("editUserWatchers_" + componentId);
    }

    public void addComponentWatcherUsers(final long componentId, final String username) {
        gotoEditUserWatchersScreen(componentId);
        tester.getDialog().setFormParameter("watcherField", username);
        tester.getDialog().submit("add_watchers");
    }

    public void addComponentWatcherUsers(final String projectKey, final long componentId, final String username) {
        gotoProjectComponentWatchers(projectKey);
        addComponentWatcherUsers(componentId, username);
    }

    public void removeComponentWatcherUser(final String projectKey, final long componentId, final String username) throws Exception {
        gotoProjectComponentWatchers(projectKey);
        gotoEditUserWatchersScreen(componentId);

        WebForm watcherForm = getFormByName("deleteWatchersForm");
        watcherForm.setCheckbox("removeWatcher_" + username, true);
        watcherForm.submit();
    }

    public void removeComponentWatcherGroup(final String projectKey, final long componentId, final String group) throws Exception {
        gotoProjectComponentWatchers(projectKey);
        tester.clickLink("editGroupWatchers_" + componentId);

        WebForm watcherForm = getFormByName("deleteWatchersForm");
        watcherForm.setCheckbox("removeWatcher_" + group, true);
        watcherForm.submit();
    }

    protected void createCustomField(final String name) {
        backdoor.customFields().createCustomField(name, null, FIELD_TYPE, FIELD_SEARCH_TYPE);
    }

    protected void editUsername(final String existingUsername, final String newUsername) {
        navigation.gotoResource("UserBrowser.jspa");
        tester.clickLink("edituser_link_" + existingUsername);

        tester.setFormElement("username", newUsername);
        tester.clickButton("user-edit-submit");
    }

    protected void flushMailQueue() {
        backdoor.mailServers().flushMailQueue();
    }

    protected WebForm getFormById(String id) throws Exception {
        WebForm[] forms = form.getForms();

        for(WebForm form : forms){
            if(form.getID().equals(id))
                return form;
        }

        throw new Exception("No form found with id " + id);
    }

    protected WebForm getFormByName(String formName) throws Exception {
        WebForm[] forms = form.getForms();

        for(WebForm form : forms){
            if(form.getName().equals(formName))
                return form;
        }

        throw new Exception("No form found with name " + formName);
    }

    @SuppressWarnings("SameParameterValue")
    protected void gotoNotificationScheme(String scheme){
        navigation.gotoResource("ViewNotificationSchemes.jspa");
        tester.clickLinkWithText(scheme);
    }

    protected void addNotificationToScheme(final long eventTypeId, final String notificationType) {
        gotoNotificationScheme("Default Notification Scheme");
        tester.clickLink("add_" + Long.toString(eventTypeId));
        tester.checkRadioOption("type", notificationType);
        tester.submit("Add");
    }

    protected void gotoProjectAdminByKey(String key) {
        navigation.gotoPage("/plugins/servlet/project-config/" + key);
    }

    @SuppressWarnings("SameParameterValue")
    protected void gotoProjectComponentWatchers(String projectKey){
        gotoProjectAdminByKey(projectKey);
        navigation.clickLinkWithExactText(COMPONENT_WATCHERS_LINK);
    }

    public void setComponentWatcherNotificationType(final String notificationTypeCode) throws Exception {
        navigation.gotoResource("ComponentWatcherSettings.jspa");
        tester.clickLink("edit_component_watcher_settings");

        WebForm watcherForm = getFormByName("component_watcher_settings_form");
        watcherForm.setParameter("notificationType", notificationTypeCode);
        watcherForm.submit();
    }

    @SuppressWarnings("SameParameterValue")
    protected void toggleAutoWatch(boolean turnOn) throws Exception {
        navigation.gotoAdmin();
        navigation.gotoResource("EditUserDefaultSettings!default.jspa");
        WebForm form = getFormById("edit_user_defaults");
        form.setCheckbox("autoWatch", turnOn);
        form.submit();
    }

    protected void toggleEmailUserOnOwnChanges(boolean enabled) {
        navigation.gotoAdmin();
        navigation.gotoResource("EditUserDefaultSettings!default.jspa");
        if(enabled) {
            tester.checkCheckbox("emailUser", "true");
        } else {
            tester.uncheckCheckbox("emailUser");
        }
        tester.clickButton("edit_user_defaults-submit");
    }
}
