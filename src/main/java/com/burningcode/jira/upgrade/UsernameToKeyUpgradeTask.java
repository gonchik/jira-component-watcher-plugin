package com.burningcode.jira.upgrade;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.burningcode.jira.plugin.JIRAComponentUserWatcher;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rbarham on 8/27/16.
 */
@ExportAsService
@Component
public class UsernameToKeyUpgradeTask implements PluginUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UsernameToKeyUpgradeTask.class);

    private static final String OLD_ENTITY_NAME = "componentUserWatcher";

    @Override
    public int getBuildNumber() {
        return 1;
    }

    @Override
    public String getShortDescription() {
        return "Converts component user watchers stored in the database from usernames to keys.";
    }

    protected String convertUsernameToKey(final UserManager userManager, final String username) {
        ApplicationUser user = userManager.getUserByName(username);
        String key = null;

        if (user != null) {
            key = user.getKey();
        } else {
            user = userManager.getUserByKey(username);

            if (user != null) {
                key = username;
            }
        }

        return key;
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        log.warn("Performing upgrade of usernames to keys.");

        Collection<Message> messages = new ArrayList<>();

        final UserManager userManager = ComponentAccessor.getUserManager();
        final ReindexMessageManager reindexMessageManager = ComponentAccessor.getComponent(ReindexMessageManager.class);
        final ProjectComponentManager projectComponentManager = ComponentAccessor.getProjectComponentManager();

        Map<String, String> userCache = new HashMap<>();

        JIRAComponentUserWatcher oldComponentUserWatcher = new JIRAComponentUserWatcher(OLD_ENTITY_NAME, userManager, reindexMessageManager);
        JIRAComponentUserWatcher newComponentUserWatcher = new JIRAComponentUserWatcher(userManager, reindexMessageManager);

        for (ProjectComponent projectComponent : projectComponentManager.findAll()) {
            log.warn("Upgrading component watchers for '" + projectComponent.getName() + "' in project '" + projectComponent.getProjectId() + "'.");

            String messagePrefix = "[Project: '" + projectComponent.getProjectId() + "', Component: '" + projectComponent.getName() + "'] ";

            oldComponentUserWatcher.setComponentId(projectComponent.getId());
            newComponentUserWatcher.setComponentId(projectComponent.getId());

            for (String username : oldComponentUserWatcher.getDatabaseIds()) {
                log.warn("{} Checking username '{}'.",messagePrefix, username);

                String newDatabaseValue;

                if (userCache.containsKey(username)) {
                    newDatabaseValue = userCache.get(username);
                    log.debug("{} Using cached value for username '{}': {}.",messagePrefix, username, newDatabaseValue);
                } else {
                    newDatabaseValue = convertUsernameToKey(userManager, username);
                    userCache.put(username, newDatabaseValue);
                }

                if (newDatabaseValue == null) {
                    log.warn("{} Could not match '{}' with a username or user key.", messagePrefix, username);
                    continue;
                }

                if (username.equals(newDatabaseValue)) {
                    log.warn("{} Username '{}' already matches user key.  Upgrade not needed.",messagePrefix, username);
                } else {
                    log.warn("{} Converting username '{}' to user key '{}'.",messagePrefix, username, newDatabaseValue);
                }

                newComponentUserWatcher.assignWatcher(newDatabaseValue);
            }
        }

        return messages;
    }

    @Override
    public String getPluginKey() {
        return "com.burningcode.jira.plugin.jiracomponentwatcher";
    }
}
