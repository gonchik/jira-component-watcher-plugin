package com.burningcode.jira.plugin.settings;

import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class NotificationTypeSetting {
    public static final String NO_NOTIFICATION_CODE = "none";

    private final String value;
    private final String name;

    public NotificationTypeSetting(final String name, final String value) {
        this.name = name;
        this.value = value;
    }

    public static String defaultCode() {
        return com.atlassian.jira.notification.type.NotificationType.ALL_WATCHERS.dbCode();
    }

    public static NotificationTypeSetting defaultSetting(final NotificationTypeManager notificationTypeManager) {
        final String code = defaultCode();
        return from(code, notificationTypeManager);
    }

    public static NotificationTypeSetting noNotificationType() {
        return new NotificationTypeSetting("None", "none");
    }

    public static NotificationTypeSetting from(final String dbCode, final NotificationType notificationType) {

        return new NotificationTypeSetting(notificationType.getDisplayName(), dbCode);
    }

    public static NotificationTypeSetting from(final String dbCode, final NotificationTypeManager notificationTypeManager) {
        if(dbCode.equals(NO_NOTIFICATION_CODE)) {
            return noNotificationType();
        }

        NotificationType notificationType = notificationTypeManager.getNotificationType(dbCode);
        if(notificationType == null) {
            return null;
        }

        return new NotificationTypeSetting(notificationType.getDisplayName(), dbCode);
    }

    public static List<NotificationTypeSetting> all(final NotificationTypeManager notificationTypeManager) {
        ArrayList<NotificationTypeSetting> notificationTypeSettings = new ArrayList<>();
        notificationTypeSettings.add(noNotificationType());

        for(Map.Entry<String, NotificationType> entry : notificationTypeManager.getSchemeTypes().entrySet()) {
            notificationTypeSettings.add(NotificationTypeSetting.from(entry.getKey(), entry.getValue()));
        }
        notificationTypeSettings.sort(Comparator.comparing(NotificationTypeSetting::getName));

        return notificationTypeSettings;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
