/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.notification.IssueEventNotificationFilterContext;
import com.atlassian.jira.notification.JiraNotificationReason;
import com.atlassian.jira.notification.NotificationFilterContext;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.burningcode.jira.plugin.customfields.ComponentWatchersFieldType;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * This is an abstract class that is used to facilitate the adding, removing, and retrieving of watchers
 * from the JIRA database.
 *
 * @author Ray Barham
 * @see com.atlassian.jira.web.action.JiraWebActionSupport
 */
@SuppressWarnings("WeakerAccess")
public abstract class JIRAComponentAbstractWatcher<T> extends JiraWebActionSupport {
    private static final Logger LOG = Logger.getLogger(JIRAComponentAbstractWatcher.class);
    private static final long serialVersionUID = 1L;
    protected static final String REMOVE_WATCHER_PREFIX = "removeWatcher_";

    private Long componentId;
    private PropertySet propertySet;
    private String watcherField;

    private final ReindexMessageManager reindexMessageManager;
    private final String entityName;
    private final String watcherFieldEntity;

    /**
     * Constructor for this JIRAComponentAbstractWatcher class.
     *
     * @param entityName         Entity name used in writing to the database.
     * @param watcherFieldEntity The type of watcher.  This serves no purpose at present but should correspond to the type of watcher the inherited class adds.
     */
    public JIRAComponentAbstractWatcher(final String entityName, final String watcherFieldEntity, final ReindexMessageManager reindexMessageManager) {
        this.entityName = entityName;
        this.watcherFieldEntity = watcherFieldEntity;
        this.reindexMessageManager = reindexMessageManager;
    }

    protected Iterable<NotificationRecipient> addWatchersAsRecipients(NotificationFilterContext context, Iterable<NotificationRecipient> intendedRecipients, Collection<ApplicationUser> watchers) {
        try {
            if (context.get(getEntityName()) == null && context.getReason() == JiraNotificationReason.ISSUE_EVENT) {
                IssueEvent issueEvent = ((IssueEventNotificationFilterContext) context).getIssueEvent();
                NotificationType notificationType = ((IssueEventNotificationFilterContext) context).getNotificationType();

                if (canSendNotifications(issueEvent, notificationType) && canSendNotificationWithNewRule(issueEvent)) {
                    context.put(getEntityName(), true);
                    Set<NotificationRecipient> currentRecipients = Sets.newHashSet();
                    Iterables.addAll(currentRecipients, intendedRecipients);

                    Collection<NotificationRecipient> notificationRecipients = usersToNotificationRecipients(watchers);

                    removeNotificationForEventUser(issueEvent, intendedRecipients, notificationRecipients);

                    if (!notificationRecipients.isEmpty()) {
                        currentRecipients.addAll(notificationRecipients);
                    }
                    return currentRecipients;
                }
            }
        } catch (RuntimeException e) {
            LOG.error(String.format("Could not add component watchers as a notification recipient: '%s'", e.getMessage()), e);
        }
        return intendedRecipients;
    }

    private static boolean canSendNotificationWithNewRule(final IssueEvent issueEvent){
        Issue issue = issueEvent.getIssue();
        ApplicationUser assignee = issue.getAssignee();
        Collection<ProjectComponent> components = issue.getComponents();

        Boolean flag= false;
        for (ProjectComponent component : components) {
            if (component.getComponentLead().equals(assignee)) {
                LOG.debug("Temporary enable to send");
                flag = true;
            }
        }
        return flag;
    }

    protected boolean canSendNotifications(final IssueEvent issueEvent, final NotificationType notificationType) {
        String notificationTypeSetting = ComponentWatcherSettings.getNotificationTypeCode();


        return issueEvent != null &&
                notificationType != null &&
                notificationTypeSetting != null &&
                issueEvent.isSendMail() &&
                notificationType.dbCode().equals(notificationTypeSetting);
    }

    /**
     * Assign a watcher to a component
     *
     * @param watcherName the name of the watcher to assign
     */
    public void assignWatcher(final String watcherName) {
        getPropertySet().setString(watcherName, watcherFieldEntity);
    }

    /**
     * Unassign a watcher to a component
     *
     * @param watcherName the name of the watcher to unassign.
     * @return If the watcher was successfully unassigned or not.  If false, watcher is not already assigned.
     */
    public boolean unassignWatcher(final String watcherName) {
        if (getPropertySet().exists(watcherName)) {
            getPropertySet().remove(watcherName);
            return true;
        }
        return false;
    }

    /**
     * Abstract method called when the user chooses to add a watcher.
     * <p>
     * Here is an example of the URL when this method is called from within velocity...
     * http://localhost:8080/secure/project/EditComponentUserWatcher!addWatchers.jspa?componentId=10000
     *
     * @return The status of adding the watchers. Returns PERMISSION_VIOLATION_RESULT if user does not have permission to add watchers.
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
    public abstract String doAddWatchers() throws EntityNotFoundException;

    /**
     * Default action performed.
     *
     * @return The status of the actions.  Returns PERMISSION_VIOLATION_RESULT if user does not have permission to add watchers.
     * @throws Exception
     */
    @Override
    public String doDefault() throws Exception {
        if (hasPermissions()) {
            initAction();
            return super.doDefault();
        }
        return PERMISSION_VIOLATION_RESULT;
    }

    /**
     * Action performed on execute.
     *
     * @return The status of the actions.  Returns PERMISSION_VIOLATION_RESULT if user does not have permission to add watchers.
     * @throws Exception
     */
    @Override
    protected String doExecute() throws Exception {
        if (hasPermissions()) {
            initAction();
            return super.doExecute();
        }
        return PERMISSION_VIOLATION_RESULT;
    }

    /**
     * Abstract method called when the user chooses to remove a watcher.
     * <p>
     * Here is an example of the URL when this method is called from within velocity...
     * http://localhost:8080/secure/project/EditComponentUserWatcher!addWatchers.jspa?componentId=10000
     *
     * @return The status of removing the watchers. Returns PERMISSION_VIOLATION_RESULT if user does not have permission to add watchers.
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
    public abstract String doRemoveWatchers() throws EntityNotFoundException;

    /**
     * Returns the ProjectComponent object of the current component id.
     *
     * @return ProjectComponent object of the current component Id.
     * @throws EntityNotFoundException
     */
    public ProjectComponent getComponent() throws EntityNotFoundException {
        return ComponentAccessor.getProjectComponentManager().find(getComponentId());
    }

    /**
     * Returns the current component id
     *
     * @return The current component id.
     */
    public Long getComponentId() {
        return componentId;
    }

    public String getEntityName() {
        return entityName;
    }

    @Override
    protected GlobalPermissionManager getGlobalPermissionManager() {
        return ComponentAccessor.getGlobalPermissionManager();
    }

    @Override
    protected PermissionManager getPermissionManager() {
        return ComponentAccessor.getPermissionManager();
    }

    public Project getProject() throws EntityNotFoundException {
        return getProjectManager().getProjectObj(getComponent().getProjectId());
    }

    /**
     * Returns a PropertySet object used for access/modifying the component watchers.
     *
     * @return The PropertySet where the component watchers are stored.
     */
    public PropertySet getPropertySet() {
        return propertySet;
    }

    /**
     * Returns a StringUtils object used by the velocity template.
     * <p>
     * Note: This was only added because the stringUtils parameter did not seem available already.
     *
     * @return StringUtils object.
     */
    public StringUtils getStringUtils() {
        return new StringUtils();
    }

    protected abstract String getWatcherDatabaseId(final T watcher);

    /**
     * Gets the name of the field that the watchers are entered in.
     *
     * @return Name of the field being used.
     */
    public String getWatcherField() {
        return watcherField;
    }

    public ReindexMessageManager getReindexMessageManager() {
        return reindexMessageManager;
    }

    /**
     * Returns a list of all the watchers.
     *
     * @return Returns a list of all the watchers as objects.
     */
    public abstract Collection<T> getWatchers();

    public Collection<String> getDatabaseIds() {
        return getPropertySet().getKeys();
    }

    public Collection<T> getWatchers(Issue issue) {
        Collection<T> watchers = new ArrayList<>();
        Long oldId = getComponentId();
        for (ProjectComponent component : issue.getComponents()) {
            setComponentId(component.getId());
            watchers.addAll(getWatchers());
        }
        setComponentId(oldId);
        return watchers;
    }

    /**
     * Used to check if the user (remote user) has admin permissions.
     *
     * @return True if the user has permissions, false otherwise.
     */
    protected boolean hasAdminPermission() {
        return getGlobalPermissionManager().hasPermission(
                GlobalPermissionKey.ADMINISTER,
                getLoggedInUser()
        ) || hasSystemAdminPermission();
    }

    protected boolean hasCustomField() throws EntityNotFoundException {
        Project project = getProject();
        Collection<CustomField> customFields = ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(project.getId(), new ArrayList<String>());
        for (CustomField customField : customFields) {
            if (customField.getCustomFieldType() instanceof ComponentWatchersFieldType) {
                return true;
            }
        }
        return false;
    }

    protected boolean hasSystemAdminPermission() {
        return getGlobalPermissionManager().hasPermission(
                GlobalPermissionKey.SYSTEM_ADMIN,
                getLoggedInUser()
        );
    }

    /**
     * Used to check if the user (remote user) has admin or project admin permissions.
     *
     * @return True if the user has permissions, false otherwise.
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
    protected boolean hasPermissions() throws EntityNotFoundException {
        return hasProjectAdminPermission() || hasAdminPermission();
    }

    protected void initAction() throws EntityNotFoundException {
        ExecutingHttpRequest.get().setAttribute("com.atlassian.jira.projectconfig.util.ServletRequestProjectConfigRequestCache:project", getProject());
    }

    /**
     * Used to check if the user (remote user) has project admin permissions.
     *
     * @return True if the user has permissions, false otherwise.
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
    protected boolean hasProjectAdminPermission() throws EntityNotFoundException {
        Project project = ComponentAccessor.getProjectManager().getProjectObj(getComponent().getProjectId());
        return getPermissionManager().hasPermission(
                ProjectPermissions.ADMINISTER_PROJECTS,
                project,
                getLoggedInUser()
        );
    }

    protected boolean isUserNotificationRecipient(ApplicationUser user, Iterable<NotificationRecipient> recipients) {
        for (NotificationRecipient recipient : recipients) {
            if (user.equals(recipient.getUser())) {
                return true;
            }
        }
        return false;
    }

    protected JiraAuthenticationContext getAuthenticationContext() {
        return ComponentAccessor.getJiraAuthenticationContext();
    }

    /**
     * Removes the user the triggered the event if they are not already set to receive a notification.
     */
    protected void removeNotificationForEventUser(IssueEvent issueEvent, Iterable<NotificationRecipient> intendedRecipients, Collection<NotificationRecipient> newRecipients) {
        ApplicationUser eventUser = issueEvent.getUser();

        if (eventUser == null) {
            return;
        }

        newRecipients.removeIf(recipient -> recipient != null && eventUser.equals(recipient.getUser()) && !isUserNotificationRecipient(eventUser, intendedRecipients));
    }

    protected boolean schemeEntitiesHaveType(final List<SchemeEntity> entities, final String type) {
        for (SchemeEntity entity : entities) {
            if (entity.getType().equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the current component id
     *
     * @param componentId A valid component id.
     */
    public void setComponentId(final Long componentId) {
        this.componentId = componentId;

        HashMap<String, Object> args = new HashMap<>();
        args.put("delegator.name", "default");
        args.put("entityName", entityName + "_" + componentId);
        args.put("entityId", 1L);

        propertySet = PropertySetManager.getInstance("ofbiz", args);
    }

    /**
     * Sets the name of the field that the watchers are entered in.
     *
     * @param watcherField The name of the field used.
     */
    public void setWatcherField(String watcherField) {
        this.watcherField = watcherField;
    }

    protected Set<NotificationRecipient> usersToNotificationRecipients(final Collection<ApplicationUser> users) {
        HashSet<NotificationRecipient> recipients = new HashSet<>();
        for (ApplicationUser user : users) {
            recipients.add(new NotificationRecipient(user));
        }
        return recipients;
    }


}