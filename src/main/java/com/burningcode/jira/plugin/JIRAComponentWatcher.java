/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import org.apache.commons.lang.StringUtils;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentComparator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.crowd.embedded.api.Group;

/**
 * Class used to display all the watchers user and group watchers for a component.
 *
 * This class returns all the components for the project and the components user and group watchers.
 * It is used to facilitate displaying information on the velocity template JIRAComponentWatcher.vm.
 *
 * @author Ray Barham
 * @see JiraWebActionSupport
 */
@SuppressWarnings("WeakerAccess")
public class JIRAComponentWatcher extends JiraWebActionSupport {
    private static final long serialVersionUID = -5750941171220722700L;

    private Project project;

    private final JIRAComponentUserWatcher componentUserWatcher;
    private final JIRAComponentGroupWatcher componentGroupWatcher;

    /**
     * Default constructor for the class
     */
    public JIRAComponentWatcher() {
        ReindexMessageManager reindexMessageManager = ComponentAccessor.getComponent(ReindexMessageManager.class);
        GroupPickerSearchService groupPickerSearchService = ComponentAccessor.getComponent(GroupPickerSearchService.class);

        componentUserWatcher = new JIRAComponentUserWatcher(ComponentAccessor.getUserManager(), reindexMessageManager);
        componentGroupWatcher = new JIRAComponentGroupWatcher(reindexMessageManager);
    }

    /**
     * Default action performed.
     *
     * @return The status of the actions.  Returns PERMISSION_VIOLATION_RESULT if user does not a project or jira admin.
     * @throws Exception
     */
    @Override
    public String doDefault() throws Exception {
        if (hasProjectAdminPermission() || hasAdminPermission()) {
            return super.doDefault();
        }
        return PERMISSION_VIOLATION_RESULT;
    }

    /**
     * Execute action.
     *
     * @return The status of the actions.  Returns PERMISSION_VIOLATION_RESULT if user does not a project or jira admin.
     * @throws Exception
     */
    @Override
    protected String doExecute() throws Exception {
        if (hasProjectAdminPermission() || hasAdminPermission()) {
            ExecutingHttpRequest.get().setAttribute("com.atlassian.jira.projectconfig.util.ServletRequestProjectConfigRequestCache:project", getProject());
            return super.doExecute();
        }
        return PERMISSION_VIOLATION_RESULT;
    }

    public Collection<ApplicationUser> getAllUsersWatching(Issue issue) {
        Collection<ApplicationUser> watchers = new ArrayList<>();
        if(issue != null) {
            for(ProjectComponent component : issue.getComponents()) {
                watchers.addAll(getAllUsersWatching(component.getId()));
            }
        }
        return watchers;
    }

    /**
     * Returns a list of all users, even those in groups, that are watching this component with the passed id.
     *
     * @param componentId the component id watchers are associated to.
     * @return A collection of all users watching.
     */
    public Collection<ApplicationUser> getAllUsersWatching(Long componentId) {
        Collection<ApplicationUser> userWatchers = getUserWatchers(componentId);
        Collection<ApplicationUser> groupWatchers = ComponentAccessor.getUserUtil().getAllUsersInGroups(getGroupWatchers(componentId));

        if(groupWatchers != null) {
            for (ApplicationUser user : groupWatchers) {
                if (!userWatchers.contains(user)) {
                    userWatchers.add(user);
                }
            }
        }

        return userWatchers;
    }

    /**
     * Returns a sorted list of all the components for the project Id.
     *
     * @return List of components.
     */
    public Collection<ProjectComponent> getComponents() {
    	Collection<ProjectComponent> components = getProject().getProjectComponents();
        ((List<ProjectComponent>) components).sort(ProjectComponentComparator.INSTANCE);

        return components;
    }

    /**
     * Returns a list of all the groups watching a component.
     *
     * @param componentId Id of the component to get watchers for.
     * @return List of watchers.
     */
    public Collection<Group> getGroupWatchers(Long componentId) {
        componentGroupWatcher.setComponentId(componentId);
        Collection<Group> objects = componentGroupWatcher.getWatchers();
        return new ArrayList<>(objects);
    }

    /**
     * Returns a Project object of the project being accessed
     *
     * @return Project object.
     */
    public Project getProject() {
    	return project;
    }

    /**
     * Returns the project ID of the project being accessed
     *
     * @return The ID of the project.
     */
    public Long getProjectId() {
        return getProject().getId();
    }

    /**
     * Returns the project key of the project being accessed
     *
     * @return The key of the project
     */
    public String getProjectKey() {
    	return getProject().getKey();
    }

    /**
     * Returns a StringUtils object used by the velocity template.
     *
     * Note: This was only added because the stringUtils parameter did not seem available already.
     *
     * @return StringUtils object.
     */
    public StringUtils getStringUtils() {
        return new StringUtils();
    }

    /**
     * Returns a list of all the users watching a component.
     *
     * @param componentId Id of the component to get watchers for.
     * @return List of watchers.
     */
    public Collection<ApplicationUser> getUserWatchers(Long componentId) {
        componentUserWatcher.setComponentId(componentId);
        Collection<ApplicationUser> objects = componentUserWatcher.getWatchers();
        return new ArrayList<>(objects);
    }

    /**
     * Used to check if the current user (remote user) has admin permission to the JIRA instance.
     *
     * @return True if the user has permissions, false otherwise.
     */
    protected boolean hasAdminPermission() {
        return ComponentAccessor.getGlobalPermissionManager().hasPermission(
                GlobalPermissionKey.ADMINISTER,
                getLoggedInUser()
        ) || hasSystemAdminPermission();
    }

    protected boolean hasSystemAdminPermission() {
        return ComponentAccessor.getGlobalPermissionManager().hasPermission(
                GlobalPermissionKey.SYSTEM_ADMIN,
                getLoggedInUser()
        );
    }

    /**
     * Used to check if the current user (remote user) has project admin permission to the project the component belongs to.
     *
     * @return True if the user has permissions, false otherwise.
     */
    protected boolean hasProjectAdminPermission() {
        return ComponentAccessor.getPermissionManager().hasPermission(
                ProjectPermissions.ADMINISTER_PROJECTS,
                getProject(),
                getLoggedInUser()
        );
    }

    public void setProject(Project project)
    {
    	this.project = project;
    }

    /**
     * Sets the project ID.  This is set via the velocity template to the current project being accessed.
     *
     * @param pid The ID of the project.
     */
    public void setProjectId(Long pid) {
    	setProject(getProjectManager().getProjectObj(pid));
        setSelectedProjectId(pid);
    }

    /**
     * Sets the project by id
     *
     * @param key The key of the project
     */
    public void setProjectKey(String key) {
    	setProject(getProjectManager().getProjectObjByKey(key));
    	setSelectedProjectId(project.getId());
    }
}