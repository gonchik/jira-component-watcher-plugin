package com.burningcode.jira.plugin;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.burningcode.jira.notification.type.ComponentWatcherNotificationType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.Map;

@ExportAsService
@Component
@SuppressWarnings("WeakerAccess")
public class ComponentWatcherLifecycle implements InitializingBean, DisposableBean, LifecycleAware {
    private static final Logger LOG = Logger.getLogger(ComponentWatcherLifecycle.class);

    @Override
    public void destroy() {
    }

    @Override
    public void afterPropertiesSet() {
    }

    protected void disableNotificationType() {
        LOG.info("Disabling Component Watcher Plugin");
        NotificationTypeManager notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
        Map<String, NotificationType> types = notificationTypeManager.getSchemeTypes();

        String notificationLabel = ComponentWatcherNotificationType.getLabel();
        LOG.info("Using notification type label for the component watcher: " + notificationLabel);

        if (types.containsKey(notificationLabel)) {
            LOG.info("Found " + notificationLabel + " as a notification type.  Removing.");
            types.remove(notificationLabel);
            notificationTypeManager.setSchemeTypes(types);
        }

        LOG.info("Component Watcher Plugin disabled.");
    }

    protected void enableNotificationType() {
        LOG.info("Enabling Component Watcher Plugin");

        NotificationTypeManager notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
        Map<String, NotificationType> types = notificationTypeManager.getSchemeTypes();

        String notificationLabel = ComponentWatcherNotificationType.getLabel();
        LOG.info("Using notification type label for the component watcher: " + notificationLabel);

        if (types != null && !types.containsKey(notificationLabel)) {
            LOG.info("Did not find " + notificationLabel + " as a notification type.  Adding.");
            types.put(notificationLabel, new ComponentWatcherNotificationType());
            notificationTypeManager.setSchemeTypes(types);
        }

        LOG.info("Component Watcher Plugin enabled.");
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }
}
