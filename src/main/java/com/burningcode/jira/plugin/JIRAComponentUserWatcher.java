/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.notification.NotificationFilter;
import com.atlassian.jira.notification.NotificationFilterContext;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.component.multiuserpicker.UserPickerWebComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Used to access and modify user watchers for a component.
 * 
 * This is the class that handles the retrieving, adding, and removing of
 * users as watchers for the JIRA Component Watcher plug-in.
 * 
 * @author Ray Barham
 */
@Scanned
public class JIRAComponentUserWatcher extends JIRAComponentAbstractWatcher<ApplicationUser> implements NotificationFilter {
    private static final Logger LOG = LoggerFactory.getLogger(JIRAComponentUserWatcher.class);

    private static final long serialVersionUID = -7117983532714470392L;

    private static final int LATEST_STORAGE_VERSION = 2;

    private static final UserManager userManager = ComponentAccessor.getUserManager();

    public JIRAComponentUserWatcher(@ComponentImport UserManager userManager, @ComponentImport ReindexMessageManager reindexMessageManager) {
        this(getEntityName(LATEST_STORAGE_VERSION), "user", userManager, reindexMessageManager);
    }

    public JIRAComponentUserWatcher(final String entityName, @ComponentImport UserManager userManager, @ComponentImport ReindexMessageManager reindexMessageManager) {
        this(entityName, "user", userManager, reindexMessageManager);
    }

    public JIRAComponentUserWatcher(final String entityName, final String watcherFieldEntity, @ComponentImport UserManager userManager, @ComponentImport ReindexMessageManager reindexMessageManager) {
        super(entityName, watcherFieldEntity, reindexMessageManager);
    }

    public static JIRAComponentUserWatcher build(final int storageVersion, final UserManager userManager, final ReindexMessageManager reindexMessageManager) {
        if (storageVersion == LATEST_STORAGE_VERSION) {
            return new JIRAComponentUserWatcher(userManager, reindexMessageManager);
        }

        return new JIRAComponentUserWatcher(getEntityName(storageVersion), userManager, reindexMessageManager);
    }

    protected static String getEntityName(final int storageVersion) {
        String entityName = "componentUserWatcher";
        if (storageVersion != 1) {
            entityName += "V" + storageVersion;
        }
        return entityName;
    }

    /**
     * Called when users are requested to be added as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which PERMISSION_VIOLATION_RESULT is returned instead
     * @throws EntityNotFoundException
     */
	public String doAddWatchers() throws EntityNotFoundException {
        if(!this.hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        initAction();

		Collection<String> userNamesToAdd = UserPickerWebComponent.getUserNamesToAdd(this.getWatcherField());

        Collection<ApplicationUser> currentWatchers = getWatchers();
        ArrayList<String> invalidWatchers = new ArrayList<>();
        boolean isUpdated = false;

        for (String userNameToAdd : userNamesToAdd) {
            ApplicationUser newWatcher = userManager.getUserByName(userNameToAdd);

            if (newWatcher == null) {
                addErrorMessage(
                        getText("action.jiracomponentwatcher.watchers.error.invalid", new String[]{userNameToAdd})
                );
                invalidWatchers.add(userNameToAdd);
                continue;
            }

            if (!currentWatchers.contains(newWatcher)) {
                assignWatcher(newWatcher.getKey());
                isUpdated = true;
            } else {
                addErrorMessage(
                        getText("action.jiracomponentwatcher.watchers.error.dup", new String[]{userNameToAdd})
                );

                invalidWatchers.add(userNameToAdd);
            }
        }

        if(isUpdated && hasCustomField()) {
            getReindexMessageManager().pushMessage(getLoggedInUser(), "admin.notifications.jiracomponentwatcher");
        }

        // Set the form field to the invalid watchers.
        setWatcherField(StringUtils.join(invalidWatchers.toArray(), ","));

        return SUCCESS;
    }

    /**
     * Called when users are requested to be removed as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which PERMISSION_VIOLATION_RESULT is returned instead
     * @throws EntityNotFoundException
     */
	public String doRemoveWatchers() throws EntityNotFoundException {
        if(!this.hasPermissions())
            return PERMISSION_VIOLATION_RESULT;
        
        initAction();

		@SuppressWarnings("unchecked")
        ArrayList<String> userNamesToRemove = new ArrayList<>(UserPickerWebComponent.getUserNamesToRemove(ActionContext.getParameters(), REMOVE_WATCHER_PREFIX));

        boolean isUpdated = false;
        for (String userNameToRemove : userNamesToRemove) {
            ApplicationUser user = userManager.getUserByName(userNameToRemove);

            if (user != null) {
                String databaseId = user.getKey();
                if (unassignWatcher(databaseId)) {
                    isUpdated = true;
                }
            }
        }

        if (isUpdated && hasCustomField()) {
            getReindexMessageManager().pushMessage(getLoggedInUser(), "admin.notifications.jiracomponentwatcher");
        }

        return SUCCESS;
        
    }

	@Override
	public Iterable<NotificationRecipient> addRecipient(NotificationFilterContext context, Iterable<NotificationRecipient> intendedRecipients) {
        return addWatchersAsRecipients(context, intendedRecipients, this.getWatchers(context.getIssue()));
	}

	@Override
	public boolean removeRecipient(NotificationRecipient recipient, NotificationFilterContext context) {
		return false;
	}

    protected String getWatcherDatabaseId(final ApplicationUser watcher) {
        if(watcher != null) {
            return watcher.getKey();
        }
        return null;
    }

    @Override
    public Collection<ApplicationUser> getWatchers() {
        Collection<ApplicationUser> watchers = new ArrayList<>();
        Collection<String> databaseIds = getDatabaseIds();

        for (String databaseId : databaseIds) {
            ApplicationUser watcher = userManager.getUserByKey(databaseId);
            if (watcher != null) {
                watchers.add(watcher);
            }
        }

        return watchers;
    }
}