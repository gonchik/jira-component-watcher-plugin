package com.burningcode.jira.plugin.customfields;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comparator.ApplicationUserBestNameComparator;
import com.atlassian.jira.issue.customfields.converters.MultiUserConverter;
import com.atlassian.jira.issue.customfields.impl.MultiUserCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.burningcode.jira.plugin.JIRAComponentWatcher;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

@Scanned
public class ComponentWatchersFieldType extends MultiUserCFType {
    private final JIRAComponentWatcher jiraComponentWatcher;

    public ComponentWatchersFieldType(
            @ComponentImport CustomFieldValuePersister customFieldValuePersister,
            @ComponentImport GenericConfigManager genericConfigManager,
            @ComponentImport MultiUserConverter multiUserConverter,
            @ComponentImport ApplicationProperties applicationProperties,
            @ComponentImport JiraAuthenticationContext authenticationContext,
            @ComponentImport UserSearchService searchService,
            @ComponentImport FieldVisibilityManager fieldVisibilityManager,
            @ComponentImport JiraBaseUrls jiraBaseUrls,
            @ComponentImport UserBeanFactory userBeanFactory) {
        super(customFieldValuePersister, genericConfigManager, multiUserConverter, applicationProperties,
                authenticationContext, searchService, fieldVisibilityManager, jiraBaseUrls, userBeanFactory);
        jiraComponentWatcher = new JIRAComponentWatcher();
    }

    @Override
    public Collection<ApplicationUser> getValueFromIssue(CustomField field, Issue issue) {
        if(issue != null) {
            return jiraComponentWatcher.getAllUsersWatching(issue);
        }
        return new Vector<>();
    }

    @Override
    public void setDefaultValue(FieldConfig fieldConfig, Collection<ApplicationUser> value) {}

    @Override
    public void createValue(CustomField customField, Issue issue, @Nonnull Collection<ApplicationUser> value) {}

    @Override
    public void updateValue(CustomField customField, Issue issue, Collection<ApplicationUser> value) {}

    @Override
    public boolean valuesEqual(Collection<ApplicationUser> v1, Collection<ApplicationUser> v2) {
        ArrayList<ApplicationUser> watcherList1 = (v1 != null? (ArrayList<ApplicationUser>)v1 : new ArrayList<>());
        ArrayList<ApplicationUser> watcherList2 = (v2 != null? (ArrayList<ApplicationUser>)v2 : new ArrayList<>());
        watcherList1.sort(new ApplicationUserBestNameComparator());
        watcherList2.sort(new ApplicationUserBestNameComparator());

        return watcherList1.equals(watcherList2);
    }
}
