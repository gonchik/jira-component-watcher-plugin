/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.notification.NotificationFilter;
import com.atlassian.jira.notification.NotificationFilterContext;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.component.multigrouppicker.GroupPickerWebComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import webwork.action.ActionContext;

import java.util.*;

/**
 * Used to access and modify group watchers for a component.
 * 
 * This is the class that handles the retrieving, adding, and removing of
 * groups as watchers for the JIRA Component Watcher plug-in.
 * 
 * @author Ray Barham
 */
@Scanned
public class JIRAComponentGroupWatcher extends JIRAComponentAbstractWatcher<Group> implements NotificationFilter
{
    private static final long serialVersionUID = 1L;
    private static final GroupManager groupManager = ComponentAccessor.getGroupManager();
    private static final GroupPickerSearchService groupPickerSearchService = ComponentAccessor.getComponent(GroupPickerSearchService.class);

    public JIRAComponentGroupWatcher(@ComponentImport ReindexMessageManager reindexMessageManager) {
        super("componentGroupWatcher", "groupname", reindexMessageManager);
    }

    /**
     * Called when groups are requested to be added as watchers.
     *
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which PERMISSION_VIOLATION_RESULT is returned instead
     * @throws EntityNotFoundException
     */
    public String doAddWatchers() throws EntityNotFoundException {
        if(!this.hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        initAction();

        Map<?, ?> params = ActionContext.getParameters();
        if(params.containsKey("groupWatchers[]")) {
	        String[] watchers = (String[])params.get("groupWatchers[]");
	        Collection<String> groupNamesToAdd = GroupPickerWebComponent.getGroupNamesToAdd(StringUtils.join(watchers, ","));

            Collection<Group> currWatchers = getWatchers();
            ArrayList<String> invalidWatchers = new ArrayList<>();
            boolean isUpdated = false;

            for (String groupNameToAdd : groupNamesToAdd) {
                if (!groupManager.groupExists(groupNameToAdd)) {
                    addErrorMessage(
                            getText("action.jiracomponentwatcher.watchers.error.invalid", new String[]{groupNameToAdd})
                    );
                    invalidWatchers.add(groupNameToAdd);
                    continue;
                }

                Group newGroup = groupManager.getGroup(groupNameToAdd);
                if (!currWatchers.contains(newGroup)) {
                    assignWatcher(newGroup.getName());
                    isUpdated = true;
                } else {
                    addErrorMessage(
                            getText("action.jiracomponentwatcher.watchers.error.dup", new String[]{groupNameToAdd})
                    );

                    invalidWatchers.add(groupNameToAdd);
                }
            }

            if(isUpdated && hasCustomField()) {
                getReindexMessageManager().pushMessage(getLoggedInUser(), "admin.notifications.jiracomponentwatcher");
            }

            // Set the form field to the invalid watchers.
            setWatcherField(StringUtils.join(invalidWatchers.toArray(), ","));
        }

        return SUCCESS;
    }

    /**
     * Called when groups are requested to be removed as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which PERMISSION_VIOLATION_RESULT is returned instead
     * @throws EntityNotFoundException
     */
    public String doRemoveWatchers() throws EntityNotFoundException {
        if(!this.hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        initAction();

        ArrayList<String> groupNamesToRemove = new ArrayList<>(GroupPickerWebComponent.getGroupNamesToRemove(ActionContext.getParameters(), REMOVE_WATCHER_PREFIX));

        boolean isUpdated = false;
        for (String groupNameToRemove : groupNamesToRemove) {
            if (groupManager.groupExists(groupNameToRemove)) {
                Group group = groupManager.getGroup(groupNameToRemove);
                String databaseId = group.getName();
                if (unassignWatcher(databaseId)) {
                    isUpdated = true;
                }
            }
        }

        if (isUpdated && hasCustomField()) {
            getReindexMessageManager().pushMessage(getLoggedInUser(), "admin.notifications.jiracomponentwatcher");
        }

        return SUCCESS;
    }

    public Collection<Group> getGroupsToAdd() {
    	Collection<Group> currentGroups = this.getWatchers();
    	Collection<Group> groupsToAdd = groupPickerSearchService.findGroups("");
    	groupsToAdd.removeAll(currentGroups);
    	return groupsToAdd;
    }

    protected String getWatcherDatabaseId(final Group watcher) {
        if(watcher != null) {
            return watcher.getName();
        }
        return null;
    }

    @Override
    public Collection<Group> getWatchers() {
        Collection<Group> watchers = new ArrayList<>();
        Collection<String> databaseIds = getDatabaseIds();

        for (String databaseId : databaseIds) {
            if(groupManager.groupExists(databaseId)) {
    		    watchers.add(groupManager.getGroup(databaseId));
    	    } else {
                unassignWatcher(databaseId);
            }
        }

        return watchers;
    }

    @Override
	public Iterable<NotificationRecipient> addRecipient(NotificationFilterContext context, Iterable<NotificationRecipient> intendedRecipients) {
        Collection<Group> groups = this.getWatchers(context.getIssue());
        List<ApplicationUser> users = new ArrayList<>();
        for(Group group : groups){
            Collection<ApplicationUser> groupUsers = groupManager.getUsersInGroup(group);
            if(groupUsers != null) {
                users.addAll(groupUsers);
            }
        }
        return addWatchersAsRecipients(context, intendedRecipients, users);
	}

	@Override
	public boolean removeRecipient(NotificationRecipient recipient, NotificationFilterContext context) {
		return false;
	}
}