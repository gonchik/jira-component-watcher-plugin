/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.notification.type;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.notification.type.AbstractNotificationType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.burningcode.jira.plugin.JIRAComponentWatcher;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Notification type used in sending notifications to component watchers.
 *
 * @author Ray Barham
 */
@ExportAsService
@Component
public class ComponentWatcherNotificationType extends AbstractNotificationType {
    private static final Logger LOG = Logger.getLogger(ComponentWatcherNotificationType.class);

    private static final String DISPLAY_NAME = "Component Watchers";
    private static final String LABEL = "Component_Watchers";

    /**
     * Returns the name associated with this notification type.
     *
     * @return The name of this notification type.
     */
    public String getDisplayName() {
        return ComponentWatcherNotificationType.DISPLAY_NAME;
    }

    /**
     * Returns a list of all the component watchers associated with the issue that threw the event.
     *
     * @return The name of this notification type.
     */
    public List<NotificationRecipient> getRecipients(IssueEvent issueEvent, String arg1) {
        Issue issue = issueEvent.getIssue();
        LOG.debug("Using custom notification type 'ComponentWatcherNotificationType' for issue '" + issue.getKey() + "'.");
        final ApplicationUser assignee = issue.getAssignee();
        Collection<ProjectComponent> components = issue.getComponents();
        ArrayList<NotificationRecipient> recipients = new ArrayList<>();
        JIRAComponentWatcher jiraComponentWatcher = new JIRAComponentWatcher();


        for (ProjectComponent component : components) {
            if (component.getComponentLead().equals(assignee)){
                LOG.debug("Start review the related user watchers");
                Collection<ApplicationUser> usersWatching = jiraComponentWatcher.getAllUsersWatching(component.getId());
                for (ApplicationUser user : usersWatching) {
                    NotificationRecipient recipient = new NotificationRecipient(user);
                    if (!recipients.contains(recipient))
                        recipients.add(recipient);
                }
            }
        }
        return recipients;
    }

    @Override
    public String getType() {
        return "componentWatcher";
    }

    /**
     * Returns the label associated with this notification type.  The label is used in the database store and various other places.
     *
     * @return The label of this notification type.
     */
    public static String getLabel() {
        return ComponentWatcherNotificationType.LABEL;
    }
}
