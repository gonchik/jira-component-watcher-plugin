package com.burningcode.jira.rest;

import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ComponentUserWatcher implements ComponentWatcher {
    @XmlElement
    protected String watcherDatabaseId;

    @XmlElement
    protected String key;

    @XmlElement
    protected String username;

    @XmlElement
    protected String name;

    @XmlElement
    private String displayName;

    @XmlElement
    private boolean isActive;

    @XmlElement
    private String type;

    // This private constructor isn't used by any code, but JAXB requires any
    // representation class to have a no-args constructor.
    private ComponentUserWatcher() {}

    public ComponentUserWatcher(@Nonnull final String watcherDatabaseId, final ApplicationUser user) {
        this.watcherDatabaseId = watcherDatabaseId;
        if (user != null) {
            this.key = user.getKey();
            this.username = user.getUsername();
            this.name = user.getName();
            this.displayName = user.getDisplayName();
            this.isActive = user.isActive();
        }
        this.type = "user";
    }
}
