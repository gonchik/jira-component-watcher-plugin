package com.burningcode.jira.rest;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.burningcode.jira.plugin.JIRAComponentUserWatcher;
import org.apache.log4j.Logger;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ComponentWatcherService {
    private static final Logger LOG = Logger.getLogger(ComponentWatcherService.class);

    private final ProjectManager projectManager;
    private final ReindexMessageManager reindexMessageManager;
    private final UserManager userManager;

    public ComponentWatcherService() {
        this.projectManager = ComponentManager.getComponent(ProjectManager.class);
        this.reindexMessageManager = ComponentManager.getComponent(ReindexMessageManager.class);
        this.userManager = ComponentManager.getComponent(UserManager.class);
    }

    @GET
    @Path("users/{projectKey}")
    public Response getUserWatchers(@PathParam("projectKey") final String projectKey, @QueryParam("storageVersion") final int storageVersion) {
        JIRAComponentUserWatcher componentUserWatcher = getComponentUserWatcherManager(storageVersion);

        ArrayList<ComponentWatcherList> components = new ArrayList<>();

        Project project = projectManager.getProjectObjByKey(projectKey);
        if (project != null) {
            for (ProjectComponent projectComponent : project.getComponents()) {
                ArrayList<ComponentWatcher> watchers = new ArrayList<>();

                componentUserWatcher.setComponentId(projectComponent.getId());

                for (String databaseId : componentUserWatcher.getDatabaseIds()) {
                    ApplicationUser user = null;

                    if (storageVersion != 1) {
                        user = userManager.getUserByKey(databaseId);
                    }

                    watchers.add(new ComponentUserWatcher(databaseId, user));
                }

                components.add(new ComponentWatcherList(projectKey, project.getName(), projectComponent.getName(), watchers));
            }
        }

        return Response.ok(components).build();
    }

    protected JIRAComponentUserWatcher getComponentUserWatcherManager(final int storageVersion) {
        if (storageVersion == 0) {
            return new JIRAComponentUserWatcher(userManager, reindexMessageManager);
        } else {
            return JIRAComponentUserWatcher.build(storageVersion, userManager, reindexMessageManager);
        }
    }
}
