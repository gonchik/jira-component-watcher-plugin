#*
  Copyright (c) 2008, 2009, Ray Barham
  All rights reserved.
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the project nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.
  THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*#

#*
 Author: Ray Barham
 Description: This is the velocity template used to edit the watchers of a component. 
*#

#disable_html_escaping()
#parse("templates/component-watcher-macros.vm")

<html>
<head>
    <style type="text/css">
        form
        {
            margin-bottom: 0;
        }

        .content
        {
        }
        
        .control_section
        {
            width: 48%;
            float: left;
            margin-right: 2px;
            margin-left: 2px;
            border: 1px solid #bbbbbb;
        }
        
        .row
        {
            width: 100%;
        }
        
        .heading
        {
            color: #ffffff;
            background-color: #bbbbbb;
            
            text-align: center;
            width: 100%;
            font-weight: bold;
        }
        
        .subheading
        {
            background-color: #eeeeee;
            font-weight: bold;
        }
        
        .right_content
        {
            background-color: #eeeeee;
        }
        
        .delete_watcher_data
        {
            width: 80%;
            float: left;
        }
        
        .delete_watcher_control
        {
            text-align: center;
        }
        
        .add_watcher_controls
        {
            text-align: center;
        }
    </style>
    <title>${i18n.getText("action.jiracomponentwatcher.group.watchers.title")} - ${i18n.getText("action.jiracomponentwatcher.form.title")}</title>
    $webResourceManager.requireResource("com.burningcode.jira.plugin.jiracomponentwatcher:admin-resources")
    <meta name="decorator" content="alt.admin"/>
    <meta name="projectKey" content="${action.project.key}"/>
    <meta name="admin.active.tab" content="edit_component_watchers"/>
    <meta name="admin.active.section" content="atl.jira.proj.config"/>
</head>
<body>
	<section id="content" role="main">
	    <header class="aui-page-header">
	        <div class="aui-page-header-inner">
	            <div class="aui-page-header-image">
	                <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
	                    <div class="aui-avatar-inner project-config-icon48-notifications"></div>
	                </div>
	            </div>
	            <div class="aui-page-header-main">
	                <ol class="aui-nav aui-nav-breadcrumbs">
	                	<li><a href="secure/project/EditComponentWatcher.jspa?projectKey=$action.project.key">${i18n.getText("action.jiracomponentwatcher.form.title")}</a></li>
	                    <li class="aui-nav-selected">${i18n.getText("action.jiracomponentwatcher.group.watchers.title")}</li>
	                </ol>
	                <h1>${i18n.getText("action.jiracomponentwatcher.group.watchers.title")}</h1>
	                <p>${i18n.getText("action.jiracomponentwatcher.group.watchers.desc", ${action.component.name})}</p>
	            </div>
	            <div class="aui-page-header-actions">
	                <div class="aui-buttons">
	                    <a class="aui-button" href="$baseurl/secure/project/EditComponentUserWatcher.jspa?componentId=$action.componentId">
	                    	${i18n.getText("action.jiracomponentwatcher.edit.user.watchers")}
                    	</a>
	                </div>
	            </div>
	        </div>
	        #show_errors($errorMessages)
	    </header>
        #if($action.componentId)
            #set ($watchers = $action.watchers)
			    
		    <div class="aui-page-panel">
		        <div class="aui-page-panel-inner">
		            <section class="aui-page-panel-content">
			            <div class="control_section">
			                <div class="heading">
			                    ${i18n.getText("action.jiracomponentwatcher.group.watchers.remove.heading")}
			                </div>
			                <div class="content">
			                    #if(!$watchers.isEmpty())
			                        <script>
			                            function setCheckboxes()
			                            {
			                                var value = document.deleteWatchersForm.removeAllWatchers.checked;
			                                var numelements = document.deleteWatchersForm.elements.length;
			                                var item;
			                                for (var i = 0 ; i < numelements ; i ++)
			                                {
			                                    item = document.deleteWatchersForm.elements[i];
			                                    item.checked = value;
			                                }
			                            }
			                        </script>
			                        <form name="deleteWatchersForm"
			                            action="$baseurl/secure/project/EditComponentGroupWatcher!removeWatchers.jspa?componentId=$action.componentId"
			                         method="post">
			                            <div class="row">
			                                <div class="subheading">
			                                    <div class="delete_watcher_data">
			                                        ${i18n.getText("action.jiracomponentwatcher.group.watchers.remove.subheading")}
			                                    </div>
			                                    <div class="delete_watcher_control">
			                                        <input type="checkbox" id="removeAllWatchers" name="removeAllWatchers" onClick="setCheckboxes()"/>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                #set ($i = 0)
			                                #foreach($watcher in $watchers)
			                                    #if($i%2 == 0)
			                                        <div class="rowNormal">
			                                    #else
			                                        <div class="rowAlternate">
			                                    #end
			                                    #set ($i = $i + 1)
			                                        <div class="delete_watcher_data">
			                                            ${watcher.name}
			                                        </div>
			                                        <div class="delete_watcher_control">
			                                            <input type="checkbox" id="removeWatcher_${watcher.name}" name="removeWatcher_${watcher.name}" />
			                                        </div>
			                                    </div>
			                                #end
			                            </div>
			                            <div class="row">
			                                <div class="delete_watcher_data">
			                                    &nbsp;
			                                </div>
			                                <div class="delete_watcher_control">
			                                    <input type="submit" name="remove_watchers" value="${i18n.getText("action.jiracomponentwatcher.watchers.remove")}"/>
			                                </div>
			                            </div>
			                        </form>
			                    #else
			                        <div class="row">
			                            ${i18n.getText("action.jiracomponentwatcher.group.watchers.remove.none")}
			                        </div>
			                    #end
			                </div>
			            </div>
			            <div class="control_section">
			                <div class="heading">
			                    ${i18n.getText("action.jiracomponentwatcher.group.watchers.add.heading")}
			                </div>
			                <div class="content right_content">
			                    <form name="addWatcherForm"
			                        action="$baseurl/secure/project/EditComponentGroupWatcher!addWatchers.jspa?componentId=$action.componentId"
			                        method="post">
			                        ${i18n.getText("action.jiracomponentwatcher.group.watchers.add.info1")}
			                        <br/><br/>
			                        ${i18n.getText("action.jiracomponentwatcher.group.watchers.add.info2")}
			                        <br/>
			                        ## macro(groupPicker $action $fieldName $fieldValue $multiselect $imageName $style)
			                        ## groupPicker($action "addWatcherForm" "watcherField" $action.watcherField "true" "" "")
			                        ## #groupPicker($action "watcherField" "" "true" "" "")
			                        
			                        #set ($groupsToAdd = $action.groupsToAdd)
			                        <select id="groupWatchers" size="7" multiple="" name="groupWatchers[]">
			                        	#foreach($group in $groupsToAdd)
			                        		<option value="${group.name}">${group.name}</option>
			                        	#end
				                	</select>
			                       	<br/>
			                       	<br/>
			                        <div class="add_watcher_controls">
			                            <input type="submit" name="add_watchers" value="${i18n.getText("action.jiracomponentwatcher.watchers.add")}"/>
			                        </div>
			                    </form>
			                </div>
			            </div>
					</section>
				</div>
			</div>
        #else
			<div class="aui-message warning">
			    <p class="title">
			        <span class="aui-icon icon-warning"></span>
			        <strong>No component id provided</strong>
			    </p>
			    <p>Something went wrong.  No component id was provided.  This should never happen.</p>
			</div>					
        #end
	</section>
</body>
</html>