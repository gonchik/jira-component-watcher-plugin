function initDbUpgradeProjectTable(projectKey) {
    var dest = projectTable(projectKey);

    requestProject(projectKey, function(results) {
        var html = '';

        results.forEach(function(currentValue){
            html += AJS.format('<tr data-component="{0}">', currentValue.name);

            html += AJS.format('<td headers="component">{0}</td>', currentValue.name);
            html += '<td headers="before-username"><span class="loading aui-icon aui-icon-wait">Loading...</span></td>';
            html += '<td headers="current-username"><span class="loading aui-icon aui-icon-wait">Loading...</span></td>';

            html += '</tr>';
        });

        dest.find('tbody')
            .empty()
            .append(html);

        initUserWatchers(dest, projectKey, "1");
        initUserWatchers(dest, projectKey);
    });
}

function initUserWatchers(projectElement, projectKey, storageVersion) {
    requestUserWatchers(projectKey, storageVersion, function(results){
        results.forEach(function(currentValue){
            var componentRow = projectElement.find(AJS.format('tr[data-component="{0}"]', currentValue['component']));
            var s1 = [];
            var s2 = [];

            currentValue.watchers.forEach(function(watcher) {
                if (storageVersion == "1") {
                    s1.push(watcher['watcherDatabaseId']);
                } else {
                    s2.push(watcher['username'])
                }
            });

            if(s1.length > 0) {
                componentRow
                    .find('td[headers=before-username]')
                    .append(s1.join('<br/>'));
            }

            if(s2.length > 0) {
                componentRow
                    .find('td[headers=current-username]')
                    .append(s2.join('<br/>'));
            }

            componentRow.find('td[headers] .loading').hide();
        });
    });
}

function isUpdated(element) {
    return AJS.$(element).attr('data-is-updated') == 'true';
}

function getProjectKeys() {
    return AJS.$('.db-upgrade-project-report').map(function(){
        return AJS.$(this).attr('data-project-key');
    });
}

function hideAllProjectTables() {
    AJS.$('.db-upgrade-project-report').hide();
}

function projectTable(projectKey) {
    return AJS.$(AJS.format('table[data-project-key={0}]', projectKey));
}

function requestProject(projectKey, success, async) {
    var url = AJS.contextPath() + AJS.format("/rest/api/2/project/{0}/components", projectKey);

    if(async == undefined) {
        async = true;
    }

    AJS.$.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        async: async,
        success: success,
        error: function(jqXHR, textStatus, errorThrown ) {
            var msg = AJS.format("[ERROR] Could not request projects: Status={0}, error={1}", textStatus, errorThrown);
            console.log(msg);
        }
    });
}

function requestUserWatchers(projectKey, storageVersion, success, async) {
    var url = AJS.contextPath() + AJS.format("/rest/componentwatchers/latest/users/{0}", projectKey);

    if(storageVersion != undefined) {
        url += AJS.format("?storageVersion={0}", storageVersion);
    }

    if(async == undefined) {
        async = true;
    }

    AJS.$.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        async: async,
        success: success,
        error: function(jqXHR, textStatus, errorThrown ) {
            var msg = AJS.format("[ERROR] Could not request user watchers: Status={0}, error={1}", textStatus, errorThrown);
            console.log(msg);
        }
    });
}

function toggleDbUpdateProject(projectKeys) {
    hideAllProjectTables();
    projectKeys.forEach(function (projectKey) {
        projectTable(projectKey).show();
    });
}

AJS.toInit(function() {
    AJS.$("#db-upgrade-project-select").auiSelect2({
        placeholder: 'Select a project'
    });

    // Prevent the project select form from submitting if enter is pressed
    AJS.$('#db-upgrade-project-select-form').submit(function(e){
        e.preventDefault();
    });

    AJS.$('#db-upgrade-project-select').change(function(){
        // Prevent reload warning dialog from displaying if page is reloaded
        AJS.$('#db-upgrade-project-select-form').trigger('submit');

        var projectKeys = AJS.$(this).val();

        if (projectKeys != undefined) {
            projectKeys.forEach(function (projectKey) {
                console.log(AJS.format('Project "{0}" selected', projectKey));
                var project = projectTable(projectKey);
                if (!isUpdated(project)) {
                    initDbUpgradeProjectTable(projectKey);
                }
                project.attr('data-is-updated', true);
            });
            toggleDbUpdateProject(projectKeys);
        } else {
            hideAllProjectTables();
            //AJS.$('#db-upgrade-project-select-form .select2-input').blur();
        }

        return false;
    });

    AJS.$('#db-upgrade-report-download').click(function() {
        AJS.$(this).prop('disabled', true);

        var projects = getProjectKeys();

        var csvContent = "data:text/csv;charset=utf-8,";
        csvContent += '"Project Key","Component Name","Status","Username"';

        projects.each(function(_, projectKey) {
            var data = [];
            requestUserWatchers(projectKey, 1, function(results1){
                results1.forEach(function(value){
                    value.watchers.forEach(function(watcher) {
                        data.push(AJS.format('"{0}","{1}","{2}","{3}"', projectKey, value.component, 'before upgrade', watcher.watcherDatabaseId));
                    })
                });
            }, false);
            requestUserWatchers(projectKey, null, function(results1){
                results1.forEach(function(value){
                    value.watchers.forEach(function(watcher) {
                        data.push(AJS.format('"{0}","{1}","{2}","{3}"', projectKey, value.component, 'current', watcher.username));
                    })
                });
            }, false);
            csvContent += data.join("\n") + "\n";
        });
        var encodedUri = encodeURI(csvContent);

        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "jcwp_database_upgrade.csv");
        document.body.appendChild(link); // Required for FF

        link.click(); // This will download the data file named "my_data.csv".

        AJS.$(this).prop('disabled', false);
    });
});
